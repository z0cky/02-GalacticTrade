
const location="localhost";// Server(API) location - can also be an IP address
const port="3000";//Server (API) port

const apiUrlBase=`http://${location}:${port}/api`;

const authPrefix="/auth";
const groupPrefix="/groups";
const categoryPrefix="/categories";
const orderPrefix="/orders";
const productPrefix="/products";
const imgPrefix="/img/";//forward slash after because it has an image name parameter

const create="/create";
const get="/get";
const edit="/edit/";//forward slash after because it has an id parameter
const destroy="/delete/";//forward slash after because it has an id parameter


//get route options
const all="/all";
const id="/id/";//forward slash after because it has an id parameter
const live="/live";
const top="/top";
const total="/total";


export const environment = {
  production: false,

  //auth routes
  loginUrl:apiUrlBase+authPrefix+"/login",
  currentUserUrl:apiUrlBase+authPrefix+"/user",
  registerUrl:apiUrlBase+authPrefix+"/register",

  //group routes
  allGroupsUrl:apiUrlBase+groupPrefix+get+all,
  groupByIdUrl:apiUrlBase+groupPrefix+get+id,
  createGroupUrl:apiUrlBase+groupPrefix+create,
  editGroupUrl:apiUrlBase+groupPrefix+edit,
  deleteGroupUrl:apiUrlBase+groupPrefix+destroy,

  //category routes
  categoryByIdUrl:apiUrlBase+categoryPrefix+get+id,
  createCategoryUrl:apiUrlBase+categoryPrefix+create,
  editCategoryUrl:apiUrlBase+categoryPrefix+edit,
  deleteCategoryUrl:apiUrlBase+categoryPrefix+destroy,

  //product routes
  livesearchProductUrl:apiUrlBase+productPrefix+get+live,
  productByIdUrl:apiUrlBase+productPrefix+get+id,
  createProductUrl:apiUrlBase+productPrefix+create,
  editProductUrl:apiUrlBase+productPrefix+edit,
  deleteProductUrl:apiUrlBase+productPrefix+destroy,

  //order routes
  livesearchOrderUrl:apiUrlBase+orderPrefix+get+live,
  orderByIdUrl:apiUrlBase+orderPrefix+get+id,
  orderTotalUrl:apiUrlBase+orderPrefix+get+total,
  orderTopUrl:apiUrlBase+orderPrefix+get+top,
  createOrderUrl:apiUrlBase+orderPrefix+create,


  //image route
  imageUrl:apiUrlBase+imgPrefix


};

