import { DetailsComponent } from './details/details.component';
import { OverviewComponent } from './overview/overview.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AddNewProductComponent } from './add-new-product/add-new-product.component';
import { CartComponent } from './cart/cart.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductInfoComponent } from './product-info/product-info.component';
import {AuthGuard} from "./guards/auth.guard";


const routes: Routes = [
  { path: 'signin', component: LoginComponent},
  { path: '', component: ProductsComponent},
  { path: 'cart', component: CartComponent},
  { path: 'productInfo/:id', component: ProductInfoComponent},
  { path: 'add-new-product', component: AddNewProductComponent,canActivate:[AuthGuard]},
  { path: 'checkout', component: CheckoutComponent},
  { path: 'product-info', component: ProductInfoComponent},
  { path: 'overview', component: OverviewComponent,canActivate:[AuthGuard]},
  { path: 'details/:id', component: DetailsComponent,canActivate:[AuthGuard]},
  {path:'**',redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
