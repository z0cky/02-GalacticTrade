import {Component, Inject, OnInit} from '@angular/core';
import {GroupService} from "../services/group.service";
import {ProductService} from "../services/product.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {catchError, first} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})

export class ProductUpdateComponent implements OnInit {
  get groupService(): GroupService {
    return this._groupService;
  }

  get productService(): ProductService {
    return this._productService;
  }

  public defaultImg:string;

  public imgUrl="./assets/choose.png";
  public editProductForm:FormGroup | null=null;
  public imgFile:File | null;

  constructor(
    private _productService:ProductService,
    private formBuilder:FormBuilder,
    private _groupService:GroupService,
    public dialogRef:MatDialogRef<ProductUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any) {

  }

  public chooseImg(event):void{
    if(event.target.files){
      const check = new FileReader();
      this.imgFile=event.target.files[0];
      check.readAsDataURL(event.target.files[0]);
      check.onload = (event$:any)=>{
        this.imgUrl = event$.target.result;
      };
    }
  }

  ngOnInit(): void {
    this.productService.getProductById(this.data.id)
      .pipe(first())
      .subscribe((product) => {
        this.editProductForm=this.formBuilder.group({
          name:[product.name,[Validators.required]],
          CategoryId:[product.CategoryId,[Validators.required]],
          price:[product.price,[Validators.required,Validators.pattern('[0-9]*')]],
          description:[product.description ?? "",[Validators.nullValidator]],
          discount:[product.discount ?? 0,[Validators.nullValidator]]
        });
        if(product.img){
          this.imgUrl=environment.imageUrl+product.img;
          this.defaultImg=environment.imageUrl+product.img;
        }
        else {
          this.defaultImg='./assets/choose.png';
        }
      });
  }

  public clearImg():void{
    this.imgUrl=this.defaultImg;
    this.imgFile=null;
  }

  public editProductFormSubmit():void{
    if(!this.editProductForm.valid)
    {
      window.alert("Invalid input");
      return;
    }

    const data=this.editProductForm.value;
    data.price=Number.parseFloat(data.price);
    data.discount=Number.parseInt(data.discount,10);

    const formData:FormData=new FormData();

    Object.entries(data).forEach(entry=>{
      formData.append(entry[0],entry[1].toString());
    });

    if(this.imgFile){
      formData.append('img',this.imgFile,this.imgFile.name);
    }

    // formData.append('id',this.data.id);

    const result:Observable<any>=this.productService.updateProduct(this.data.id,formData);
    result.pipe(first(),catchError(err => {
      window.alert(err.message);
      return of();
    })).subscribe(object => {
      window.alert(object.message);
      this.productService.setAll();
      this.dialogRef.close();
    });
  }
}
