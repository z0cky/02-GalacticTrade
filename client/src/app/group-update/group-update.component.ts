import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GroupService} from "../services/group.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {first} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-group-update',
  templateUrl: './group-update.component.html',
  styleUrls: ['./group-update.component.css']
})
export class GroupUpdateComponent implements OnInit {

  constructor(private formBuilder:FormBuilder,
              private _groupService:GroupService,
              public dialogRef:MatDialogRef<GroupUpdateComponent>,
              @Inject(MAT_DIALOG_DATA) public data:any) { }

  public editGroupForm:FormGroup | null=null;

  get groupService(): GroupService {
    return this._groupService;
  }

  ngOnInit(): void {
    this.groupService.getGroupById(this.data.id)
      .pipe(first())
      .subscribe(group => {
        this.editGroupForm=this.formBuilder.group({
          name:[group.name,[Validators.required]],
          description:[group.description ?? "",[Validators.nullValidator]]
        });
      });
  }

  public editGroupFormSubmit():void{
    if(!this.editGroupForm.valid)
    {
      window.alert("Invalid input");
      return;
    }

    const data=this.editGroupForm.value;
    const id=this.data.id;

    const result:Observable<any>=this.groupService.updateGroup(id,data);
    result.pipe(first()).subscribe(object => {
      window.alert(object.message);
      this.groupService.setAll();
      this.dialogRef.close();
    });
  }

}
