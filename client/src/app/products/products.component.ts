import { Product } from './../models/product.model';
import { AuthService } from './../services/auth.service';
import { Component, OnChanges, OnInit, SimpleChanges, EventEmitter, ViewChild, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import { ProductService } from '../services/product.service';
import { Input } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog} from "@angular/material/dialog";
import {ProductUpdateComponent} from "../product-update/product-update.component";
import {take} from "rxjs/operators";
import {environment} from '../../environments/environment'


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})


export class ProductsComponent implements OnInit{
  get authService(): AuthService {
    return this._authService;
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  public dataSource: MatTableDataSource<Product>;

  public readonly environment=environment;

  constructor(private _authService: AuthService,public dialog: MatDialog ,private _productService: ProductService, private changeDetectorRef: ChangeDetectorRef) {}

  get productService(): ProductService {
    return this._productService;
  }

  onSearchChange(newValue: string): void {
    this._productService.searchText = newValue;
    this.refreshProducts();
  }
  onMinPriceChange(newValue: number): void {
    this._productService.minPrice = newValue;
    this.refreshProducts();
  }
  onMaxPriceChange(newValue: number): void {
    this._productService.maxPrice = newValue;
    this.refreshProducts();
  }
  onWithDiscountChange(newValue: boolean): void {
    this._productService.withDiscount = newValue;
    this.refreshProducts();
  }
  onSortChange(newValue:string):void{
    this._productService.sort=this.sortStringToObject(newValue);
    this.refreshProducts();
  }

  resetCategory() {
    this._productService.selectedCategoryId = null;
    this._productService.selectedCategoryName = "All";
    this.refreshProducts();
  }

  public dataSourceSub:Subscription=null;

  resetFilters() {
    this._productService.searchText = null;
    this._productService.maxPrice = null;
    this._productService.minPrice = null;
    this._productService.sort=null;
    this.minPrice = null;
    this.maxPrice = null;
    this.withDiscount = null;
    this.search = null;
    this.sortString="";
    this.refreshProducts();
  }


  public refreshProducts():void {
    this._productService.setAll();
  }


  ngOnInit() {
    this.changeDetectorRef.detectChanges();
    this.productService.paginator=this.paginator;
    this.refreshProducts();
  }

  ngOnDestroy() {
    if (this.productService.dataSource) {
      this.productService.dataSource.disconnect();
    }
  }

  private sortStringToObject(str:string):{field:string,mode:string} | null{
    if(!str) {
      return null;
    }
    const arr=str.split(",");
    if(arr.length!==2) {
      return null;
    }
    return {field:'sort'+arr[0],mode:arr[1]};
  }

  public search: string | null;
  public minPrice: number | null;
  public maxPrice: number | null;
  public withDiscount: boolean | null;

  public sortString:string;

  addProduct(item: Product){
    item.count += 1;
  }

  itemsCart: Product[] = [];
  addProductToCart(item: Product) {
    console.log(item);
    let cartDataNull = localStorage.getItem('myData');
    if(cartDataNull == null) {
      let storeDataGet:Product[] = [];
      item.count = 1;
      storeDataGet.push(item);
      localStorage.setItem('myData', JSON.stringify(storeDataGet));
    }
    else {
      let id = item.id;
      let pointer:number = -1;
      this.itemsCart = JSON.parse(localStorage.getItem('myData'));
      for(let num = 0; num < this.itemsCart.length; num++){
        if(id === this.itemsCart[num].id){
          this.itemsCart[num].count += 1;
          pointer = num;
          break;
        }
      }
      if(pointer == -1){
        item.count = 1;
        this.itemsCart.push(item);
        localStorage.setItem('myData', JSON.stringify(this.itemsCart));
      }
      else {
        localStorage.setItem('myData', JSON.stringify(this.itemsCart));
      }
    }
    this.cartNumUpdate();
  }

  cartNumber:number = 0;
  cartNumUpdate(){
    var cartValue = JSON.parse(localStorage.getItem('myData'));
    this.cartNumber = cartValue.length;
    this._authService.myCartSub.next(this.cartNumber);
  }

  public editProductForm(id:number):void{
    this.dialog.open(ProductUpdateComponent,{data:{id}});
  }

  public deleteProduct(id:number):void{
    this._productService.deleteProduct(id)
      .pipe(take(1))
      .subscribe(response => {
        // window.alert(response.message);
        this.refreshProducts();
      });
  }

}


