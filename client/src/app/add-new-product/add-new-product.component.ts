import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GroupService} from "../services/group.service";
import {Observable} from "rxjs";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-add-new-product',
  templateUrl: './add-new-product.component.html',
  styleUrls: ['./add-new-product.component.css']
})
export class AddNewProductComponent implements OnInit {
  get groupService(): GroupService {
    return this._groupService;
  }
  get productService(): ProductService {
    return this._productService;
  }


  public imgUrl="./assets/choose.png";
  public addProductForm:FormGroup;
  public imgFile:File | null=null;

  public addGroupForm:FormGroup;
  public addCategoryForm:FormGroup;

  constructor(private _productService:ProductService, private formBuilder:FormBuilder,private _groupService:GroupService) {
    this.addProductForm=this.formBuilder.group({
      name:['',[Validators.required]],
      CategoryId:['',[Validators.required]],
      price:['',[Validators.required,Validators.pattern('[0-9]*')]],
      description:['',[Validators.nullValidator]]
    });
    this.addCategoryForm=this.formBuilder.group({
      name:['',[Validators.required]],
      GroupId:['',[Validators.required]],
      description:['',[Validators.nullValidator]]
    });
    this.addGroupForm=this.formBuilder.group({
      name:['',[Validators.required]],
      description:['',[Validators.nullValidator]]
    });
  }

  ngOnInit(): void {

  }
  public chooseImg(event):void{
    if(event.target.files){
      const check = new FileReader();
      this.imgFile=event.target.files[0];
      check.readAsDataURL(event.target.files[0]);
      check.onload = (event$:any)=>{
        this.imgUrl = event$.target.result;
      };
    }
  }

  public clearImg():void{
    this.imgUrl="./assets/choose.png";
    this.imgFile=null;
  }

  public addProductFormSubmit():void{
    if(!this.addProductForm.valid)
    {
      window.alert("Invalid input");
      return;
    }

    const data=this.addProductForm.value;
    data.price=Number.parseFloat(data.price);

    const formData:FormData=new FormData();

    Object.entries(data).forEach(entry=>{
      formData.append(entry[0],entry[1].toString());
    });



    if(this.imgFile){
      formData.append('img',this.imgFile,this.imgFile.name);
    }



    const result:Observable<any>=this.productService.createProduct(formData);
    result.pipe(first()).subscribe(object => {
      window.alert(object.message);
    });
  }

  public addCategoryFormSubmit():void{
    if(!this.addCategoryForm.valid)
    {
      window.alert("Invalid input");
      return;
    }

    const data=this.addCategoryForm.value;

    const result:Observable<any>=this.groupService.createCategory(data);
    result.pipe(first()).subscribe(object => {
      window.alert(object.message);
      this.groupService.setAll();
    });
  }

  public addGroupFormSubmit():void{
    if(!this.addGroupForm.valid)
    {
      window.alert("Invalid input");
      return;
    }

    const data=this.addGroupForm.value;

    const result:Observable<any>=this.groupService.createGroup(data);
    result.pipe(first()).subscribe(object => {
      window.alert(object.message);
      this.groupService.setAll();
    });
  }

}
