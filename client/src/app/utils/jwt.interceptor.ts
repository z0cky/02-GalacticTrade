import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to api url
    if (localStorage.getItem('access-token')) {
      request = request.clone({headers:request.headers.set('Authorization',`Bearer ${localStorage.getItem('access-token')}`)});
    }

    return next.handle(request);
  }
}
