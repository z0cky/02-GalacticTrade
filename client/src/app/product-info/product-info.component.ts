import {Component, OnDestroy, OnInit} from '@angular/core';
import { AuthService } from './../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';
import {Observable, Subscription} from "rxjs";
import {Product} from "../models/product.model";
import {Location} from "@angular/common";
import {environment} from '../../environments/environment'

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit , OnDestroy{

  constructor(private _authService: AuthService,private _actRoute: ActivatedRoute, private _productService: ProductService,private location:Location) {
    this.paramMapSub=this._actRoute.paramMap.subscribe(value => {
      this.productObservable=this.productService.getProductById(value.get('id'));
    });
  }
  public readonly environment=environment;

  get productService() : ProductService {
    return this._productService;
  }

  get authService(): AuthService {
    return this._authService;
  }


  get actRoute(): ActivatedRoute {
    return this._actRoute;
  }

  private paramMapSub:Subscription;
  public productObservable:Observable<Product>;
  ngOnDestroy(): void {
    if(this.paramMapSub!==null){
      this.paramMapSub.unsubscribe();
    }
  }

  public back():void{
    this.location.back();
  }

  ngOnInit(): void {
  }

  cartNumber:number = 0;
  cartNumUpdate(){
    var cartValue = JSON.parse(localStorage.getItem('myData'));
    this.cartNumber = cartValue.length;
    this._authService.myCartSub.next(this.cartNumber);
  }

  itemsCart: Product[] = [];
  addProductToCart(item: Product) {
    console.log(item);
    let cartDataNull = localStorage.getItem('myData');
    if(cartDataNull == null) {
      let storeDataGet:Product[] = [];
      item.count = 1;
      storeDataGet.push(item);
      localStorage.setItem('myData', JSON.stringify(storeDataGet));
    }
    else {
      let id = item.id;
      let pointer:number = -1;
      this.itemsCart = JSON.parse(localStorage.getItem('myData'));
      for(let num = 0; num < this.itemsCart.length; num++){
        if(id === this.itemsCart[num].id){
          this.itemsCart[num].count += 1;
          pointer = num;
          break;
        }
      }
      if(pointer == -1){
        item.count = 1;
        this.itemsCart.push(item);
        localStorage.setItem('myData', JSON.stringify(this.itemsCart));
      }
      else {
        localStorage.setItem('myData', JSON.stringify(this.itemsCart));
      }
    }
    this.cartNumUpdate();
  }

  addProduct(item: Product){
    item.count += 1;
  }
}
