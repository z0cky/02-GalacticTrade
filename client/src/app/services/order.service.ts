import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Order} from "../models/order.model";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Product} from "../models/product.model";
import {first, take} from "rxjs/operators";
import {OrderItem} from "../models/order-item.model";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http:HttpClient) {
    this.setAll();
  }

  private readonly baseOrdersUrl: string = environment.livesearchOrderUrl;
  private readonly totalOrderValueUrl: string = environment.orderTotalUrl;
  private readonly topProductsUrl: string = environment.orderTopUrl+"?top=3";
  private readonly getOrderItemsUrl: string = environment.orderByIdUrl;

  public searchText: string | null;
  public startDate:Date | null;
  public endDate:Date | null;

  private _orders:Observable<Order[]>;

  private _totalOrderValue:Observable<any>;

  private _topProducts:Observable<Product[]>;



  get topProducts(): Observable<Product[]> {
    return this._topProducts;
  }

  get totalOrderValue(): Observable<any> {
    return this._totalOrderValue;
  }

  public getOrderItems(id: string):Observable<OrderItem[]>{
    return this.http.get<OrderItem[]>(this.getOrderItemsUrl+id.toString());
  }

  public setTotalOrderValue():void {
    this._totalOrderValue = this.http.get(this.totalOrderValueUrl);
  }
  public setTopProducts():void{
    this._topProducts=this.http.get<Product[]>(this.topProductsUrl);
  }

  get orders(): Observable<Order[]> {
    return this._orders;
  }

  get paginatedOrders(): Observable<Order[]> {
    return this._paginatedOrders;
  }

  // get totalOrderValue():Observable<any>{
  //   return this.http.get(this.totalOrderValueUrl);
  // }

  public paginator: MatPaginator;
  public dataSource: MatTableDataSource<Order>;
  private _paginatedOrders:Observable<Order[]>;

  public setAll(): void {
    let url = this.baseOrdersUrl + "?";
    if(this.searchText) {
      url += "search=" + this.searchText + "&";
    }
    if(this.startDate) {
      url += "startDate=" + this.startDate + "&";
    }
    if(this.endDate) {
      url += "endDate=" + this.endDate + "&";
    }
    this._orders = this.http.get<Order[]>(url);

    this.orders.pipe(take(1)).subscribe(orders => {
      this.dataSource=new MatTableDataSource<Order>(orders);
      this.dataSource.paginator=this.paginator;
      this._paginatedOrders=this.dataSource.connect();
    });
  }

}
