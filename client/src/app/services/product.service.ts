import { HttpClient } from '@angular/common/http';
import {Injectable, ViewChild} from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category.model';
import { Product } from '../models/product.model';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {first, take} from "rxjs/operators";
import {AuthService} from "./auth.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  get paginatedProducts(): Observable<Product[]> {
    return this._paginatedProducts;
  }

  constructor(private http:HttpClient) {
    this.setAll();
  }

  private _products:Observable<Product[]>;
  private readonly baseProductsUrl: string = environment.livesearchProductUrl;
  private readonly baseProductInfoUrl: string = environment.productByIdUrl;
  private readonly createProductUrl:string=environment.createProductUrl;
  private readonly updateProductUrl:string=environment.editProductUrl;
  private readonly createOrderUrl:string=environment.createOrderUrl;
  private readonly deleteProductUrl: string = environment.deleteProductUrl;

  public paginator: MatPaginator;
  private _paginatedProducts: Observable<Product[]>;
  public dataSource: MatTableDataSource<Product>;


  public selectedCategoryId: number;
  public selectedCategoryName: string = "All";
  public searchText: string | null;
  public minPrice: number | null;
  public maxPrice: number | null;
  public withDiscount: boolean | null;
  //field:sortName or sortPrice --- mode:ASC or DESC
  public sort:{field:string,mode:string} | null;



  public get products(): Observable<Product[]> {
    return this._products;
  }

  public getProductById(id: string):Observable<Product>{
    return this.http.get<Product>(this.baseProductInfoUrl+id);
  }

  public createProduct(data:FormData):Observable<any>{
    return this.http.post(this.createProductUrl,data);
  }

  public updateProduct(id:number,data:FormData):Observable<any>{
    return this.http.put(this.updateProductUrl+id.toString(),data);
  }

  public createOrder(data:any):Observable<any>{
    return this.http.post(this.createOrderUrl,data);
  }

  public deleteProduct(id:number):Observable<any>{
    return this.http.delete(this.deleteProductUrl+id.toString());
  }

  public setAll(): void {
    let url = this.baseProductsUrl + "?";
    if(this.selectedCategoryId) {
      url += "CategoryId=" + this.selectedCategoryId + "&";
    }
    if(this.searchText) {
      url += "search=" + this.searchText + "&";
    }
    if(this.minPrice) {
      url += "minPrice=" + this.minPrice + "&";
    }
    if(this.maxPrice) {
      url += "maxPrice=" + this.maxPrice + "&";
    }
    if(this.withDiscount){
      url += "withDiscount=" + this.withDiscount + "&";
    }
    if(this.sort){
      url+= this.sort.field+"=" + this.sort.mode + "&";
    }
    this._products = this.http.get<Product[]>(url);

    this.products.pipe(first()).subscribe(products => {
      this.dataSource = new MatTableDataSource<Product>(products);
      this.dataSource.paginator = this.paginator;
      this._paginatedProducts = this.dataSource.connect();
    });
  }
}
