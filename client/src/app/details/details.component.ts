import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {OrderService} from "../services/order.service";
import {Observable} from "rxjs";
import {OrderItem} from "../models/order-item.model";
import {take} from "rxjs/operators";
import {Location} from "@angular/common";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  get actRoute(): ActivatedRoute {
    return this._actRoute;
  }

  get orderService(): OrderService {
    return this._orderService;
  }

  public orderItems:Observable<OrderItem[]>;

  constructor(private _actRoute: ActivatedRoute,private _orderService:OrderService,private location:Location) {
    this.actRoute.paramMap.pipe(take(1))
      .subscribe(value => {
        this.orderItems=this.orderService.getOrderItems(value.get('id'));
      });
  }

  public back():void{
    this.location.back();
  }
  ngOnInit(): void {
  }

}
