export class User {
  id:number;
  username:string;
  fullname:string;
  email:string;
  isAdmin:boolean;
}
