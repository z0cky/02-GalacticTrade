import {Category} from "./category.model";
import {Observable} from "rxjs";

export class Group {
  id:number;
  name:string;
  description:string | null;
  Categories:Category[];
}
