export class Category {
  id:number;
  name:string;
  description:string | null;
  GroupId:number;
  deletable:boolean | undefined;
}
