(async function (){
    const fs=require('fs')
    const path=require('path')
    const envfile=require('envfile')
    const {promisify}=require('util')
    const {randomFill}=require('crypto')

    try {
        const fileName=path.resolve("../","../",".env")
        const secretBuffer=await promisify(randomFill)(Buffer.alloc(64))
        const secret=secretBuffer.toString("hex")
        const fileText=await promisify(fs.readFile)(fileName)
        const env=envfile.parse(fileText)
        env["COOKIE_SECRET"]=secret
        const fileTextNew=envfile.stringify(env)
        await promisify(fs.writeFile)(fileName,fileTextNew)
    } catch (e)
    {
        console.log(e.message)
    }
})();