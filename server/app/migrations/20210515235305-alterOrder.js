'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.removeColumn('Orders','price');
    await queryInterface.removeColumn('Orders', 'deliveryDate');
    await queryInterface.renameColumn('Orders', 'name', 'firstName');
    await queryInterface.renameColumn('Orders', 'surname', 'lastName');
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.renameColumn('Orders', 'firstName', 'name');
    await queryInterface.renameColumn('Orders', 'lastName', 'surname');
    await queryInterface.addColumn('Orders','deliveryDate',{
      type: Sequelize.STRING,
      allowNull: false,
    });
    await queryInterface.addColumn('Orders','price',{
      type: Sequelize.INTEGER,
      allowNull: false,
    });
  }
};
