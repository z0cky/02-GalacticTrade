const db=require('../../models');
const Order = db.Order;
const OrderItem = db.OrderItem;
const Product = db.Product;
const {ValidationError,Op,cast,where}=require('sequelize')
const nodemailer = require('nodemailer');

async function getById(req, res, next) {
    const id = req.params.id; 
    if(!id)
        return res.status(400).send({"message":"No order id provided"});
    try {
        const result = await Order.findByPk(id);
        if(!result) {
            return res.status(404).send({"message": "No such order in the database"});
        }

        const items = await result.getOrderItems({include: Product});

        return res.status(200).json(items);
    } catch (e) {
        e.status = 500;
        next(e);
    }
};

async function getAll(req, res, next){
    try {
        let result = await Order.findAll();
        return res.status(200).json(result);
    } catch (e) {
        e.status = 500;
        next(e);
    }
}

async function liveSearch(req,res,next){
    const startDate = req.query.startDate;
    const endDate = req.query.endDate;

    let search=req.query.search ?? ""
    try {
        let searchProperty={[Op.iLike]:'%'+search+'%'}
        let queryObject={
            [Op.or]:[
                {firstName:searchProperty},
                {lastName:searchProperty},
                {email:searchProperty},
                {address:searchProperty},
                {city:searchProperty},
            ]
        };
        if(startDate || endDate)
            queryObject.createdAt={};
        if(startDate){
            queryObject.createdAt[Op.gte] = new Date(startDate);
        }
        if(endDate){
            queryObject.createdAt[Op.lte] = new Date(endDate);
        }


        const results=await Order.findAll({
            where:queryObject,
            order:[['createdAt','DESC']]
        })



        for (const iterator of results) {
            iterator.dataValues.total=await iterator.total();
        }
        // console.log(results);
        return res.status(200).json(results)
    }catch (e)
    {
        e.status=500
        next(e)
    }
}

async function totalOrderValue(req,res,next) {
    try {
        const total=await (await OrderItem.findAll()).reduce(async (acc, orderItem) => {
            const product=await orderItem.getProduct();
            return (await acc)+(orderItem.count*(product.price-(product.price*product.discount/100)));
        },Promise.resolve(0));
        return res.status(200).json({value:total});
    }catch (e) {
        next(e);
    }
}

async function topSelling(req, res, next) {
    try {
        const top=req.query.top ?? 1;

        let result=await Product.findAll();
        for (const resultElement of result) {
            resultElement.setDataValue('timesSold',await resultElement.timesSold());
        }
        const topProducts=result.sort((a, b) => b.dataValues.timesSold-a.dataValues.timesSold)
            .slice(0,top);
        return res.status(200).json(topProducts);
    }catch (e) {
        next(e);
    }

}


async function addOrder(req, res, next) {
    const items = req.body.items;
    const order = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        address: req.body.address,
        city: req.body.city,
        // deliveryDate: req.body.deliveryDate,
        paymentMethod: req.body.paymentMethod,
        // price: 0,
    };

    if(!order.firstName || !order.lastName || !order.email || !order.address  || !order.city
        ||  !order.paymentMethod) {
        return res.status(400).send({"message":"Missing order information"});
    }
    try {
        const newOrder = await Order.create(order);
        for(let key in items) {
            const item = {
                ProductId: items[key].ProductId,
                count: items[key].count,
                OrderId: newOrder.id,
            }
            await OrderItem.create(item);

        }
            const transporter=nodemailer.createTransport({
                service:process.env.EMAIL_SERVICE,
                port:process.env.EMAIL_PORT,
                secure:process.env.EMAIL_SECURE,
                auth:{user: process.env.EMAIL, pass: process.env.EMAIL_PASSWORD}
            });
            let mailText="------------ORDER------------\n\n\n";
            mailText+=`Order Date:${new Date(newOrder.createdAt).toLocaleDateString('sr')}\n\n`;
            mailText+=`Name:${newOrder.firstName} ${newOrder.lastName}\n`;
            mailText+=`Address:${newOrder.address},${newOrder.city}\n`;
            mailText+=`Payment method:${newOrder.paymentMethod}\n\n\n`;
            mailText+="----------Products-----------\n"
            for (const orderItem of await newOrder.getOrderItems()) {
                const product=await orderItem.getProduct();
                mailText+="Product: "+product.name+"\n";
                mailText+="Quantity: "+orderItem.count+"\n";
                mailText+="----------------\n"
            }
            mailText+=`Total:$${await newOrder.total()}\n`;
            let info=await transporter.sendMail({
                to:newOrder.email,
                subject:"Order no."+newOrder.id,
                text:mailText
            });
        return res.status(201).json({"message" : "Sucessfully ordered the items!"});
    } catch(e) {
        e.status = 400;
        next(e);
    }
};

async function deleteOrder(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message": "No order id provided"});
    try {
        const order = await Order.findByPk(id);
        if(!order) {
            return res.status(404).send({"message": "No such order in the database"});
        }

        await OrderItem.destroy({
            where: {OrderId: id}
        });

        await order.destroy();
        return res.status(200).json({"message" : "success"});
    } catch (e) {
        e.status = 500;
        next(e);
    }
};

module.exports = {
    getById,
    getAll,
    addOrder,
    deleteOrder,
    liveSearch,
    totalOrderValue,
    topSelling
};
