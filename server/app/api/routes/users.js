const express=require('express')
const router=express.Router()
const controller=require('../controllers/userController')
const authenticated=require('../middleware/authenticated')

router.get("/get",authenticated,controller.getAll)
router.get("/get/id/:id",authenticated,controller.getById)
router.get("/get/live",authenticated,controller.liveSearch)


module.exports=router