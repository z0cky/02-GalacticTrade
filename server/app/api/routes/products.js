const express = require('express');
const router = express.Router();
const controller = require('../controllers/productController');
const authenticated = require('../middleware/authenticated');
const {uploadImage}=require('../middleware/imageUpload');


router.get('/get/id/:id', controller.getById);
router.post('/create', authenticated,uploadImage().single('img'),controller.addNewProduct);
router.delete('/delete/:id', authenticated , controller.deleteProduct);
router.put('/edit/:id', authenticated, uploadImage().single('img'),controller.editProduct);
router.get("/get/live",controller.liveSearch);
router.get("/get/price",controller.priceRange);
router.get("/get/category/:id",controller.findCategory);
router.get("/get/group/:id",controller.findGroup);

module.exports = router;
