const express=require('express');
const router=express.Router();
const authenticated=require('../middleware/authenticated');
const {getById,getProducts,getGroup,destroy,edit,create,liveSearch}=require('../controllers/categoryController');

router.get("/get/live",liveSearch);
router.get("/get/id/:id",getById);
router.get("/get/products/:id",getProducts);
router.get("/get/group/:id",getGroup);

router.post("/create",authenticated,create);

router.put("/edit/:id",authenticated,edit);

router.delete("/delete/:id",authenticated,destroy);


module.exports=router