const express=require('express');
const router=express.Router();
const passport=require('passport');
const strategy=require('../strategies/strategyJwt');
const {login,logout,authUser,register}=require('../controllers/userController');
const authenticated=require('../middleware/authenticated');
const {isAdmin}=require('../middleware/authorized');

router.post('/login',login);
router.get('/logout',authenticated,logout);
router.get('/user',authenticated,authUser);
router.post("/register",authenticated,isAdmin,register);

module.exports=router;
