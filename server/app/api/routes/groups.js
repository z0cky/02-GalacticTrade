const express = require('express');
const router = express.Router();
const authenticated = require('../middleware/authenticated');

const {getById, getProducts, createNewGroup, editGroup, deleteGroup, getCategories, getAll} = require('../controllers/groupController');

router.get("/get/id/:id", getById);
router.get("/get/all", getAll);
router.get("/get/categories/:id", getCategories);
router.get("/get/products/:id", getProducts);
router.post("/create", authenticated, createNewGroup);
router.put("/edit/:id",authenticated, editGroup);
router.delete("/delete/:id",authenticated, deleteGroup);

module.exports = router
