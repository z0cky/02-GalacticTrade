// function authenticated(req,res,next){
//     if(!req.isAuthenticated())
//         return res.status(401).json({"message":"Not logged in"})
//     return next()
// }
const passport=require('passport')
const strategy=require('../strategies/strategyJwt')
const authenticated=passport.authenticate('jwt',{session:false});

module.exports = authenticated;
