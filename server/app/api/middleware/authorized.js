function isAdmin(req,res,next) {
    if(!req.user.dataValues.isAdmin)
        return res.status(401).json({'message':"Unauthorized"});
    return next();
}

module.exports.isAdmin = isAdmin;
