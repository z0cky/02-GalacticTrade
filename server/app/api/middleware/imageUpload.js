const multer=require('multer');
const path=require('path');
const generateRandomString = (length=6)=>Math.random().toString(20).substr(2, length);


module.exports.uploadImage = ()=>{
    const storage=multer.diskStorage({
        destination:(req,file,cb)=>{cb(null,path.resolve('resources/img'))},
        filename:(req,file,cb)=>{cb(null,generateRandomString(32)+Date.now()+path.extname(file.originalname))}
    })
    return multer({storage:storage});
}

