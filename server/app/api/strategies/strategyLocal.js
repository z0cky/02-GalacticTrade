let passport=require('passport')
let LocalStrategy=require('passport-local').Strategy
const User=require('../../models').User


passport.serializeUser((user,done)=>{
    done(null,user.id)
})
passport.deserializeUser(async (id,done)=>{
    try {
        let user=await User.findByPk(id)
        done(null,user)
    }
    catch (e)
    {
        done(e)
    }
})

passport.use(new LocalStrategy(
    {},async function (username,password,done){
        try {
            let user=await User.findOne({where:{username:username}})
            if(!user)
                return done(null,false,{message:"Incorrect username"})

            let valid=await user.checkPassword(password)
            if(!valid)
                return done(null,false,{message:"Incorrect password"})
            return done(null,user)
        }catch (e)
        {
            return done(e)
        }
    }
))


module.exports=passport