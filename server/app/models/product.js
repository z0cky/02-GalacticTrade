'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Product.belongsTo(models.Category)
      models.Product.hasMany(models.OrderItem);
    }
    async getGroup(){
      const category=await this.getCategory();
      return await category.getGroup();
    }
    async timesSold(){
      return await (await this.getOrderItems()).reduce(async (acc, orderItem) => {
        return (await acc)+orderItem.count;
      },Promise.resolve(0));
    }
  };
  Product.init({
    name: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    description: DataTypes.TEXT,
    img: DataTypes.STRING,
    price: {
      type: DataTypes.FLOAT,
      validate:{
        notEmpty:true,
        min: 0
      }
    },
    discount: {
      type: DataTypes.INTEGER,
      defaultValue:0,
      validate:{
        notEmpty:true,
        min: 0,
        max: 100
      }
    },
    CategoryId: {
      type: DataTypes.INTEGER,
      validate:{
        notEmpty:true
      }
    }
  }, {
    sequelize,
    modelName: 'Product',
    timestamps:false
  });
  return Product;
};
