'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.OrderItem.belongsTo(models.Order);
      models.OrderItem.belongsTo(models.Product);
    }
  };
  OrderItem.init({
    count: {
      type: DataTypes.INTEGER,
      defaultValue:1
    },
    OrderId: {
      type: DataTypes.INTEGER,
      validate:{
        notEmpty:true
      }
    },
    ProductId: {
      type: DataTypes.INTEGER,
      validate:{
        notEmpty:true
      }
    }
  }, {
    sequelize,
    modelName: 'OrderItem',
    timestamps: false,
  });
  return OrderItem;
};
