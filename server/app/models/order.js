'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Order.hasMany(models.OrderItem)
    }
    async total(){
      const items=await this.getOrderItems();
      let total=0;
      for (const item of items) {
        const product=await item.getProduct();
        total+=item.count*(product.price-(product.price*(product.discount ?? 0)/100));
      }
      return total;
    }
  };
  Order.init({
    firstName: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    lastName: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    email: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    address: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    city: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    paymentMethod: {
      type: DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    // deliveryDate: {
    //   type: DataTypes.STRING,
    //   validate:{
    //     notEmpty:true
    //   }
    // },
    // price: {
    //   type: DataTypes.INTEGER,
    //   validate:{
    //     notEmpty:true
    //   }
    // },
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};
