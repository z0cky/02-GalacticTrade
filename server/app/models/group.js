'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Group extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Group.hasMany(models.Category)
    }
    async getProducts(){
      const categories=await this.getCategories();

      let products=[];
      for (let i=0;i<categories.length;++i)
      {
        products=products.concat(await categories[i].getProducts());
      }
      return products;
    }

  };

  Group.init({
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Group',
    timestamps: false,
  });
  return Group;
};