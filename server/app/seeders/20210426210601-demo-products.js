'use strict';

const db=require('../models');
const Category=db.Category;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const gpus=await Category.findOne({where:{name:'Graficke kartice'}});
    const cpus=await Category.findOne({where:{name:'Procesori'}});
    const motherboards=await Category.findOne({where:{name:'Maticne ploce'}});

    return await queryInterface.bulkInsert('Products',[
      {
        name:"Nvidia GTX 580",
        price:85,
        CategoryId:gpus.id,
        img:'gtx580.jpeg'
      },
      {
        name:"Nvidia GTX 1050 Ti",
        price:110,
        CategoryId:gpus.id,
        img:'gtx1050ti.jpg'
      },
      {
        name:"AMD Radeon RX 470",
        price:110,
        CategoryId:gpus.id,
        img:'rx470.jpg'
      },
      {
        name:"AMD Radeon RX 570",
        price:130,
        CategoryId:gpus.id,
        img:'rx570.jpg'
      },
      {
        name:"AMD FX-6100",
        price:50,
        CategoryId:cpus.id,
        img:'fx6100.jpeg'
      },
      {
        name:"AMD FX-4170",
        price:80,
        CategoryId:cpus.id,
        img:'fx4170.jpg'
      },
      {
        name:"MSI 760GM-P21",
        price:30,
        CategoryId: motherboards.id,
        img:'msi760gm-p21.jpg'
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('Products',{});
  }
};
