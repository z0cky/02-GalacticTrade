'use strict';

const {Op} = require("sequelize");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return await queryInterface.bulkInsert('Groups',[
      {
        name:'Racunarske komponente',
      },
      {
        name:'Desktop konfiguracije',
      },
      {
        name:'Laptop racunari'
      },
      {
        name: 'Mobilni telefoni'
      },
      {
        name: 'TV'
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('Groups',{});
  }
};
