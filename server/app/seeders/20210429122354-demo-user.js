'use strict';

const db=require('../models');
const User=db.User;
const bcrypt=require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return await queryInterface.bulkInsert("Users", [
      {
        username: "testadmin",
        fullname: "Test Admin",
        password: await bcrypt.hash("testadmin", 10),
        email: "testadmin@example.com",
        isAdmin: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: "testuser",
        fullname: "Test User",
        password: await bcrypt.hash("testuser", 10),
        email: "testuser@example.com",
        isAdmin: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete("Users",{});
  }
};
