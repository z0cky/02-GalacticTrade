#!/usr/bin/python3.8

import os
import random
import string
import sys
from termcolor import colored
import platform

from dotenv import dotenv_values


def runCmd(cmd: str, errorMsg: str = "Error!", successMsg: str = "Success!") -> None:
    if os.system(cmd) != 0:
        print(colored(errorMsg, color="red"))
        exit(1)
    else:
        print(colored(successMsg, color="green"))

if "--help" in sys.argv:
    print("Options:")
    print("--fresh:Remove all volumes(database)")
    print("-e:Set up .env file(must have .env.example)")
    print("-m:Migrate database")
    print("-s:Seed database")
    exit(0)

commBase = "docker-compose "
if platform.system().lower() == 'Linux'.lower():
    commBase = "sudo " + commBase

if "--fresh" in sys.argv:
    runCmd(commBase + "down -v", "Error removing volumes!", "Volumes removed successfully!")

if "-e" in sys.argv:
    if not os.path.exists(".env.example"):
        print(colored(".env.example file does not exist!", color="red"))
        exit(1)

    if not os.path.exists(".env") or input(
            colored('Would you like to override old .env file?[y/N]:', color="green")).lower() == 'y':
        config = dotenv_values(".env.example")
        try:
            with open('.env', "w") as env:
                config['COOKIE_SECRET'] = ''.join(
                    random.choice(string.ascii_letters + string.digits) for _ in range(128))
                print("Enter email(Gmail) from which the customer will be notified on order")
                print("Email should have less secure apps enabled,using throwaway email recommended")
                config['EMAIL'] = input("Username:")
                config['EMAIL_PASSWORD'] = input("Password:")

                for key, value in config.items():
                    env.write(f"{key}={value}\n")
        except:
            print(colored("Error writing env", color="red"))
            exit(1)

    print(colored(".env file written", color="green"))

runCmd(commBase + "build", "Error building containers!", "Containers built successfully!")

if "-m" in sys.argv:
    runCmd(commBase + "run app npx sequelize-cli db:migrate", "Error migrating!", "Migration successful!")
    os.system(commBase + "down")
if "-s" in sys.argv:
    runCmd(commBase + "run app npx sequelize-cli db:seed:all", "Error seeding!", "Seeding successful!")
    os.system(commBase + "down")


