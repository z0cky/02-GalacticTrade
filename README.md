# Project GalacticTrade

E-commerce web sajt za prodaju tehnike.

# Tehnologije
- Angular (client side)
- Node.js/Express.js (server side)
- PostgreSQL (DBMS)
- Docker,Docker Compose(kontejnerizovanje i pokretanje)

# Pokretanje aplikacije
Da bi ste pokrenuli,potrebno je da imate
-   npm
-   Docker
-   Docker compose
<hr>

### Server side

### 1. Nacin (python build script)

<pre>
- pozicionirati se u server direktorijum
- sudo apt install python3-pip
- pip3 install termcolor
- pip3 install python-dotenv
- sudo chmod +x build.py
- ./build.py -e -m -s --fresh ili od nule ./build.py -e -m -s 
  (./build.py --help da ispise komande)
- unesite username i password email-a za testiranje gde mora da bude (gmail)  
  sa koga se ce api slati mailove posle porudzbine
- sudo docker-compose up

 i API je pokrenut.

</pre>


### 2. Nacin

Prvo trebate da pokrenete server side(API).

Prvo se pozicionirate u server direktorijum,napravite fajl .env,iskopirate sadrzaj iz .env.example u .env , zatim za COOKIE_SECRET dodate kljuc za JWT autentifikaciju(moze da bude bilo kakav string),EMAIL i EMAIL_PASSWORD
stavite username i password email-a za testiranje gde mora da bude  (gmail)  sa koga se ce api slati mailove posle porudzbine (less secure apps podesavanje treba da bude ukljuceno,preporuka napraviti novi mail samo za testiranje)

Zatim u istom direktorijumu pokrenite:
`sudo docker-compose build`
da biste buildovali containere za bazu i api

Zatim pokrenite migracije:
`sudo docker-compose run app npx sequelize-cli db:migrate`

Pa spustite containere
`sudo docker-compose down`

Posle toga,pokrenite seeder za bazu(Dodavanje podataka)
`sudo docker-compose run app npx sequelize-cli db:seed:all`
Ponovo spustite containere
`sudo docker-compose down`

Posle toga pokrenite:
`sudo docker-compose up`

I API je pokrenut
<hr>

### Client side

Za dizajn je koriscena Angular Material biblioteka.

Pozicionirajte se u client direktorijum, i u slucaju da nije instalirano pokrenite
`sudo npm install -g @angular/cli`

Zatim pokrenite
`npm install`
da bi se svi potrebni paketi instalirali

Na kraju,da bi ste pokrenuli klijentsku aplikaciju,pokrenite
`npm start` ili n `ng serve`

I u ovom trenutku,klijentska aplikacija je pokrenuta
<hr>

### Inicijalni podaci u bazi
Database seeder dodaje nekoliko grupa,kategorija,i proizvoda u bazu.
Pored toga,dodaje i 2 korisnika:
- Test Admin(username:testadmin, password:testadmin)
- Test User(username:testuser, password:testuser)

## Dijagram baze
![](Galactic_Trade_DB_Diagram.png)

<hr>

<a name ="video"></a>
### Desktop version of the website<br>
![](video/desktop_demo.mp4)

### Mobile version of the website<br>
![](video/compressed_mobile.mp4)

## Developers

- [Zoran Vujicic, 158/2015](https://gitlab.com/z0cky)
- [Katarina Popovic, 361/2020](https://gitlab.com/katarina.popovic)
- [Viktor Gizdavic, 146/2017](https://gitlab.com/ArthasWasRight)
- [Lazar Lukic, 52/2017](https://gitlab.com/Baja-KS)
